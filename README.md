### Running instructions
1. Run:
```
mvn clean package
java -jar target/railway-client-0.1.0.jar
```
2. Visit: `http://localhost:8080/customers`

### Note
To let application run correctly there need to be REST API exposed at URLs specified in `application.yml` file implementing interfaces under `pl.codilingus.railway.backend`.