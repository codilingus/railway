package pl.codilingus.railway.backend;

import java.util.List;
import java.util.Optional;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import pl.codilingus.railway.model.Customer;

@Service
@FeignClient(name = "customers", url = "${backend.url}", path = "customers")
public interface CustomerService {

  @GetMapping
  List<Customer> findAll(@RequestParam(name = "lastName", required = false, defaultValue = "") String lastName);

  @GetMapping(path = "/{id}")
  Optional<Customer> findById(@PathVariable("id") int id);

  @PostMapping
  void save(@RequestBody Customer customer);

  @PutMapping("/{id}")
  void update(@PathVariable("id") int id, @RequestBody Customer customer);

  @DeleteMapping("/{id}")
  void delete(@PathVariable("id") int id);

  @GetMapping("/count")
  int count();

}
