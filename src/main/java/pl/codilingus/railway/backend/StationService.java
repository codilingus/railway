package pl.codilingus.railway.backend;

import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Service
@FeignClient(name = "stations", url = "${backend.url}", path = "stations")
public interface StationService {

  @GetMapping("/all")
  List<String> findAll();

  @GetMapping("/from/{stationName}")
  List<String> findDestinationsFrom(@PathVariable("stationName") String from);

}
