package pl.codilingus.railway.backend;

import java.util.List;
import java.util.Optional;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import pl.codilingus.railway.model.Customer;
import pl.codilingus.railway.model.Order;

@Service
@FeignClient(name = "orders", url = "${backend.url}", path = "orders")
public interface OrderService {

  @GetMapping
  List<Order> findAll();

  @GetMapping(path = "/{id}")
  Optional<Order> findById(@PathVariable("id") int id);

  @PostMapping
  void save(@RequestBody Order order);

  @DeleteMapping("/{id}")
  void delete(@PathVariable("id") int id);

}
