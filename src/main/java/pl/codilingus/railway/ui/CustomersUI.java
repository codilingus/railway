package pl.codilingus.railway.ui;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import pl.codilingus.railway.backend.CustomerService;
import pl.codilingus.railway.model.Customer;

@SpringUI(path = "/customers")
public class CustomersUI extends UI {

  private final CustomerService customerService;
  private final CustomerEditor customerEditor;

  private final Grid<Customer> customersTable;
  private final TextField customerLastNameFilter;
  private final Button addNewCustomerButton;

  @Autowired
  public CustomersUI(CustomerService customerService, CustomerEditor customerEditor) {
    this.customerService = customerService;
    this.customerEditor = customerEditor;

    this.customersTable = new Grid<>(Customer.class);
    this.customerLastNameFilter = new TextField();
    this.addNewCustomerButton = new Button("New customer", VaadinIcons.PLUS);
  }

  @Override
  protected void init(VaadinRequest request) {
    buildLayout();

    buildCustomersTable();
    buildCustomerLastNameFilter();
    buildAddCustomerButton();

    customerEditor.setActionButtonCallback(this::refreshCustomersTable);

    refreshCustomersTable();
  }

  private void buildAddCustomerButton() {
    addNewCustomerButton.addClickListener(e -> customerEditor.editCustomer(new Customer("", "")));
  }

  private void buildCustomersTable() {
    customersTable.setWidth(525, Unit.PIXELS);
    customersTable.setColumns("id", "firstName", "lastName");
    customersTable.asSingleSelect().addValueChangeListener(e -> customerEditor.editCustomer(e.getValue()));
  }

  private void buildCustomerLastNameFilter() {
    customerLastNameFilter.setPlaceholder("Filter by last name");
    customerLastNameFilter.setValueChangeMode(ValueChangeMode.LAZY);
    customerLastNameFilter.addValueChangeListener(e -> refreshCustomersTable());
  }

  private Button navigationButton() {
    Button button = new Button("Go to orders", VaadinIcons.ARROW_BACKWARD);
    button.addClickListener(e -> getPage().setLocation("/orders"));
    return button;
  }

  private void buildLayout() {
    GridLayout actions = new GridLayout(2, 1, customerLastNameFilter, addNewCustomerButton);
    actions.setSizeFull();
    actions.setComponentAlignment(customerLastNameFilter, Alignment.MIDDLE_LEFT);
    actions.setComponentAlignment(addNewCustomerButton, Alignment.MIDDLE_RIGHT);
    VerticalLayout customersLayout = new VerticalLayout(navigationButton(), actions, customersTable, customerEditor);
    customersLayout.setWidth(600, Unit.PIXELS);
    setContent(customersLayout);
  }

  private void refreshCustomersTable() {
    customersTable.setItems(customerService.findAll(customerLastNameFilter.getValue()));
  }

}
