package pl.codilingus.railway.ui;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import pl.codilingus.railway.backend.CustomerService;
import pl.codilingus.railway.backend.OrderService;
import pl.codilingus.railway.backend.StationService;
import pl.codilingus.railway.model.Customer;
import pl.codilingus.railway.model.Order;

@SpringUI(path = "/orders")
public class OrdersUI extends UI {

  private final CustomerService customerService;
  private final OrderService orderService;
  private final StationService stationService;

  private final Grid<Order> ordersTable;
  private ComboBox<String> stationFromComboBox;
  private ComboBox<String> destinationComboBox;
  private ComboBox<Customer> customerComboBox;

  @Autowired
  public OrdersUI(OrderService orderService, CustomerService customerService, StationService stationService) {
    this.orderService = orderService;
    this.customerService = customerService;
    this.stationService = stationService;

    this.ordersTable = new Grid<>(Order.class);
    this.customerComboBox = new ComboBox<>("Select customer");
    this.stationFromComboBox = new ComboBox<>("Select starting point");
    this.destinationComboBox = new ComboBox<>("Select destination");
  }

  @Override
  protected void init(VaadinRequest request) {
    buildLayout();
    refreshOrdersTable();
  }

  private Grid<Order> buildOrdersTable() {
    ordersTable.setColumns("id", "customerId", "from", "to");
    return ordersTable;
  }

  private Layout buildOrderPlacingLayout() {
    return new VerticalLayout(
        createCustomerComboBox(),
        createFromComboBox(),
        this.destinationComboBox,
        createOrderButton()
    );
  }

  private ComboBox<Customer> createCustomerComboBox() {
    customerComboBox.setItems(customerService.findAll(""));
    customerComboBox.setItemCaptionGenerator(customer -> customer.getFirstName() + " " + customer.getLastName());
    return customerComboBox;
  }

  private ComboBox<String> createFromComboBox() {
    stationFromComboBox.setItems(stationService.findAll());
    stationFromComboBox.addValueChangeListener(e -> {
      destinationComboBox.clear();
      destinationComboBox.setItems();
      if (e.getValue() != null && !e.getValue().isEmpty()) {
        destinationComboBox.setItems(stationService.findDestinationsFrom(e.getValue()));
      }
    });
    return stationFromComboBox;
  }

  private Button createOrderButton() {
    Button orderButton = new Button("Order!");
    orderButton.addClickListener(e -> {
      if (!customerComboBox.isEmpty() && !stationFromComboBox.isEmpty() && !destinationComboBox.isEmpty()) {
        try {
          orderService.save(new Order(customerComboBox.getValue().getId(), stationFromComboBox.getValue(), destinationComboBox.getValue()));
        } catch (FeignException ex) {
          String errorMessage = ex.getMessage().split("\n")[1];
          Notification notification = new Notification(errorMessage, Type.ERROR_MESSAGE);
          notification.setDelayMsec(5000);
          notification.show(this.getPage());
        }
        refreshOrdersTable();
      } else {
        Notification notification = new Notification("Please fill all order fields", Type.WARNING_MESSAGE);
        notification.setDelayMsec(3000);
        notification.show(this.getPage());
      }
    });
    return orderButton;
  }

  private Button navigationButton() {
    Button button = new Button("Go to customers", VaadinIcons.ARROW_BACKWARD);
    button.addClickListener(e -> getPage().setLocation("/customers"));
    return button;
  }

  private void buildLayout() {
    VerticalLayout customersLayout = new VerticalLayout(navigationButton(), buildOrderPlacingLayout(), buildOrdersTable());
    customersLayout.setWidth(600, Unit.PIXELS);
    setContent(customersLayout);
  }

  private void refreshOrdersTable() {
    ordersTable.setItems(orderService.findAll());
  }

}
