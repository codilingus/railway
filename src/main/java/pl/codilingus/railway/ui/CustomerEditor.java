package pl.codilingus.railway.ui;

import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import pl.codilingus.railway.backend.CustomerService;
import pl.codilingus.railway.model.Customer;

@UIScope
@SpringComponent
public class CustomerEditor extends VerticalLayout {

  private final CustomerService repository;
  private final Binder<Customer> binder = new Binder<>(Customer.class);
  private Customer customer;

  private final TextField firstName = new TextField("First name");
  private final TextField lastName = new TextField("Last name");

  private final Button save = new Button("Save", VaadinIcons.CHECK);
  private final Button cancel = new Button("Cancel", VaadinIcons.STOP);
  private final Button delete = new Button("Delete", VaadinIcons.TRASH);


  private Runnable buttonClickListener = () -> { };

  @Autowired
  public CustomerEditor(CustomerService repository) {
    this.repository = repository;
    binder.bindInstanceFields(this);
    initComponents();
  }

  private void initComponents() {
    CssLayout actions = new CssLayout(save, cancel, delete);
    actions.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

    save.setStyleName(ValoTheme.BUTTON_PRIMARY);
    save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
    setActionButtonListeners();

    setVisible(false);
    addComponents(firstName, lastName, actions);
  }

  public void setActionButtonCallback(Runnable handler) {
    this.buttonClickListener = handler;
  }

  public void editCustomer(Customer customer) {
    if (Objects.isNull(customer)) {
      setVisible(false);
      return;
    }

    this.customer = customer;
    binder.setBean(this.customer);
    setVisible(true);
    save.focus();
    firstName.focus();
  }

  private void setActionButtonListeners() {
    ClickListener defaultClickListener = e -> {
      this.customer = null;
      this.setVisible(false);
      this.buttonClickListener.run();
    };

    cancel.addClickListener(defaultClickListener);

    save.addClickListener(e -> saveOrUpdate());
    save.addClickListener(defaultClickListener);

    delete.addClickListener(e -> repository.delete(customer.getId()));
    delete.addClickListener(defaultClickListener);
  }

  private void saveOrUpdate() {
    if (Objects.nonNull(this.customer.getId())) {
      repository.update(this.customer.getId(), customer);
    } else {
      repository.save(customer);
    }
  }

}
