package pl.codilingus.railway.model;

public class Order {

  private int id;
  private int customerId;
  private String from;
  private String to;

  public Order() {
  }

  public Order(int customerId, String from, String to) {
    this.customerId = customerId;
    this.from = from;
    this.to = to;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getCustomerId() {
    return customerId;
  }

  public void setCustomerId(int customerId) {
    this.customerId = customerId;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

}
